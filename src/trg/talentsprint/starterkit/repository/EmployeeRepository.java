package trg.talentsprint.starterkit.repository;

import trg.talentsprint.starterkit.model.*;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	//Employee findByName(String name);
	@Query("select e from Employee e where e.name = ?1")
	Employee findByName(String name);
	@Query("select e from Employee e where e.location = ?1")
	Employee findByLocation(String location);
	
}
