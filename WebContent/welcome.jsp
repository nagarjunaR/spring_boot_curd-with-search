<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Create an account</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
th {
	background-color:yellow;
}
</style>
</head>
<body>
	<div class="container">
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>

			<h2>
				Welcome ${pageContext.request.userPrincipal.name} | <a
					onclick="document.forms['logoutForm'].submit()"
					style="color: blue;">Logout</a>
			</h2>
		</c:if>
		<hr />
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<div class="container-full">
		<nav class="navbar bg-secondary">
			<img src="${contextPath}/Images/logo.jpeg" class="rounded-circle" align="left"
				style="height: 120px; width: 120px" alt="not found">
			<h2>
				<i style="color: cyan;">List of Employees in "N.R Software
					Solutions"</i>
			</h2>
			<a href="${contextPath}/about"><input type="submit" name="action"
				class="btn btn-primary" value="About Us"></a> <a
				href="${contextPath}/services"><input type="submit"
				name="action" class="btn btn-primary" value="Services"></a> <a
				href="${contextPath}/careers"><input type="submit" name="action"
				class="btn btn-primary" value="Careers"></a> <img
				src="${contextPath}/Images/pic3.jpeg" class="rounded-circle" align="left"
				style="height: 120px; width: 120px" alt="not found">
		</nav>
	</div>
	<div>
		<div align="center">

			
			<form action="${contextPath}/search" method="get">
			<input type="text" name="name" placeholder="Employee Name">
				<input type="submit" class="btn btn-warning" value="search">
			</form>&nbsp;&nbsp;
			<form action="${contextPath}/locationSearch" method="get">
			<input type="text" name="location" placeholder="Employee Location">
				<input type="submit" class="btn btn-warning" value="Search By Location">
			</form>
			<br>

			<div>
				<div align="right">
					<a href="${contextPath}/new-employee">
						<button type="submit" class="btn btn-warning">Add New
							Employee</button>
					</a>
				</div>
				<div>
					<div>
						<h2 style="color: Green;">Employees
							List</h2>
					</div>
				</div>
				<div>
					<table  style="background-color: cyan;" class="table"
						class="table table-hover" border="1">
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Location</th>
							<th>Salary</th>
							<th>Update User</th>
							<th>Delete User</th>
							<th>Rating</th>
							<th>Feed back</th>
							<th>Edification</th>
						</tr>
						<c:forEach var="emp" items="${Employees}">
							<tr>
								<td>${emp.id}<br></td>
								<td>${emp.name}<br></td>
								<td>${emp.location}<br></td>
								<td>${emp.salary}<br></td>

								<td>
									<form action="${contextPath}/${emp.id}" method="get">
										<input type="submit" class="btn btn-success" value="Edit" />
									</form>
								</td>
								<td>
									<form action="${contextPath}/${emp.id}/delete" method="get">
										<input type="submit" class="btn btn-danger" value="Delete" />
									</form>
								</td>
								<td>5</td>
								<td>
									<form action="${contextPath}/comments" method="get">
										<input type="submit" class="btn btn-primary" value="Comment" />
									</form>
								</td>
								<td><form action="${contextPath}/About" method="get">
										<input type="submit" class="btn btn-primary" value="About" />
									</form></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</div>
	<br>

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<img alt="Not Found" src="${contextPath}/Images/pic1.jpeg">&nbsp;&nbsp;&nbsp;
	<img alt="Not Found" src="${contextPath}/Images/pic2.jpeg">&nbsp;&nbsp;&nbsp;
	<img alt="Not Found" src="${contextPath}/Images/pic3.jpeg">&nbsp;&nbsp;&nbsp;
	<img alt="Not Found" src="${contextPath}/Images/pic4.jpeg">&nbsp;&nbsp;&nbsp;
	<img alt="Not Found" src="${contextPath}/Images/pic5.jpeg">&nbsp;&nbsp;&nbsp;
	<img alt="Not Found" src="${contextPath}/Images/pic6.jpeg">



</body>
</html>
