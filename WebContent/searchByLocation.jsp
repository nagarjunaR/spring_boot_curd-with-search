<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Search</title>
</head>
<body>

	<h2>List of Employees you searched</h2>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Location</th>
			<th>Salary</th>

			<th>Rating</th>
			<th>Feed back</th>
			<th>Edification</th>
		</tr>
		
		<tr>
			<td>${search.id}</td>
			<td>${search.name}</td>
			<td>${search.location}</td>
			<td>${search.salary}</td>

			<td>5</td>
			<td>
				<form action="${contextPath}/comments" method="get">
					<input type="submit" class="btn btn-primary" value="Comment" />
				</form>
			</td>
			<td><form action="${contextPath}/About" method="get">
					<input type="submit" class="btn btn-primary" value="About" />
				</form></td>

		</tr>
	</table>
</body>
</html>