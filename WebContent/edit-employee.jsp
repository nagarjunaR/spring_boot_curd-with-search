<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ page isELIgnored="false"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Book Library</title>
</head>
<body>
	<center>
		<div>
			<h2>New Employee</h2>
			<div>
				<div>
					<form:form action="${contextPath}/${emp.id}/update" modelAttribute="emp"
						method="post">
						<div>
							<div>Id: ${emp.id}</div>
							<div>
								<form:label path="name">Name</form:label>
								<form:input type="text" id="name" path="name" />
								<form:errors path="name" />
							</div>
							<div>
								<form:label path="location">Location</form:label>
								<form:input type="text" id="location" path="location" />
								<form:errors path="location" />
							</div>
							<div>
								<form:label path="salary">Salary</form:label>
								<form:input type="number" id="salary" path="salary" />
								<form:errors path="salary" />
							</div>
						</div>
						<div>
							<div>
								<input type="submit" value="Update Employee">
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</center>
</body>
</html>